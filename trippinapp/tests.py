from django.test import TestCase
from .models import Register_human_user
from .models import Register_hotel_user
from .models import Register_restaurant_user
from .models import Register_attraction_user
from .models import Post
from .utils import *
# Create your tests here.

class Register_hotel_user_TestCase(TestCase):
    def setUp(self):
        hotel_name = "STRAND PALACE HOTEL"
        hotel_adress= "LON"
        hotel_email = "oi@oi.com"
        hotel_password = "oi12345@oi.com"
        hotel_amenities, hotel_price, hotel_phone_number, hotel_rating = get_hotel(hotel_name, hotel_adress)
        Register_hotel_user.objects.create(hotel_name=hotel_name, hotel_adress=hotel_adress, hotel_price=hotel_price, hotel_email=hotel_email, hotel_password = hotel_password, hotel_amenities=hotel_amenities, hotel_phone_number = hotel_phone_number, hotel_rating = hotel_rating)

    def test_hotels_information(self):
        """Animals that can speak are correctly identified"""
        hotel_rating = Register_hotel_user.objects.get(hotel_name = "STRAND PALACE HOTEL")
        print(hotel_rating)

class Register_restaurant_user_TestCase(TestCase):
    def setUp(self):
        restaurant_name = "Brunch & Cake"
        restaurant_adress= '{"lat": 41.397158,"long": 2.160873}'
        restaurant_cuisines= "vegana"
        restaurant_email = "oi@oi.com"
        restaurant_password = "oi12345@oi.com"
        restaurant_special_diets = "vegana"
        restaurant_phone_number = "666666"
        restaurant_business_hours = "16-22"
        citycode = json.loads(restaurant_adress)
        latitude = citycode["lat"]
        longitude = citycode["long"]
        tags = get_restaurant(restaurant_name, latitude, longitude)
        Register_restaurant_user.objects.create(restaurant_name=restaurant_name, restaurant_adress=restaurant_adress, restaurant_cuisines=restaurant_cuisines, restaurant_email=restaurant_email, restaurant_password=restaurant_password, restaurant_special_diets= restaurant_special_diets, restaurant_phone_number = restaurant_phone_number, restaurant_business_hours = restaurant_business_hours, tags = tags)

    def test_hotels_information(self):
        """Animals that can speak are correctly identified"""
        hotel_rating = Register_restaurant_user.objects.get(restaurant_name = "Brunch & Cake")
        print(hotel_rating)

class Register_attraction_user_TestCase(TestCase):
    def setUp(self):
        attraction_name = "Barcelona: Casa Batlló Entrance Ticket with Smart Guide"
        attraction_adress= '{"lat": 41.397158,"long": 2.160873}'
        attraction_email = "oi@oi.com"
        attraction_password = "oi12345@oi.com"
        attraction_phone_number = "666666"
        attraction_business_hours = "10-22"
        citycode = json.loads(attraction_adress)
        latitude = citycode["lat"]
        longitude = citycode["long"]
        attraction_paid = get_attraction(attraction_name, latitude, longitude)
        Register_attraction_user.objects.create(attraction_name=attraction_name, attraction_adress=attraction_adress, attraction_paid=attraction_paid, attraction_email=attraction_email, attraction_password=attraction_password, attraction_phone_number = attraction_phone_number, attraction_business_hours= attraction_business_hours)

    def test_hotels_information(self):
        """Animals that can speak are correctly identified"""
        hotel_rating = Register_attraction_user.objects.get(attraction_name = "Barcelona: Casa Batlló Entrance Ticket with Smart Guide")
        print(hotel_rating)