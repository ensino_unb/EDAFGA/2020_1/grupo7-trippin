from amadeus import Client, ResponseError
from .models import *
import json

amadeus = Client(
        client_id = 'tFWhJdA95xsaqVxBjNU5wQjIocF8xClU',
        client_secret = 'tj01onHsJjeZMetH'
    )

def get_hotel(hotel_name, hotel_cityName):
    print("'" + str(hotel_cityName) + "'")
    try:
        response = amadeus.shopping.hotel_offers.get(
            cityCode = str(hotel_cityName),
            #name = str(hotel_name)
        )
        print(response.data)
        for x in response.data:
            #Pega o nome do hotel e joga na variavel, depois compara com o que o o usuario cadastrado e printa as amenities e o preço total
            nome_hotel = x.get("hotel").get("name")
            print(nome_hotel)
            if (nome_hotel == str(hotel_name)):
                #print(nome_hotel)
                amenities = x.get("hotel").get("amenities")
                #print(x.get("hotel").get("amenities"))
                #print("\n")
                rating = x.get("hotel").get("rating")
                price = x.get("offers")[0].get("price").get("total")
                phone = x.get("hotel").get("contact").get("phone")
                image = x.get("hotel").get("media")
                print(image)
                #print(x.get("offers")[0].get("price").get("total"))
                return str(amenities), str(price), str(phone), str(rating)
            else:
                 print("Bruh")
    except ResponseError as error:
        print(error)

def get_restaurant(restaurant_name, latitude, longitude):
    try:
        response = amadeus.reference_data.locations.points_of_interest.get(
            latitude=latitude, 
            longitude=longitude)
        for x in response.data:
            
            name_restaurant = x.get("name")
            print(name_restaurant)
            if (name_restaurant == str(restaurant_name)):
                print(name_restaurant)
                tags = x.get("tags")
                return tags
            else:
                 print("Bruh")
    except ResponseError as error:
        print(error)

def get_attraction(attraction_name, latitude, longitude):
    try:
        response = amadeus.shopping.activities.get(
            #name=str(attraction_name),
            latitude=latitude, 
            longitude=longitude)
        for x in response.data:
            
            #Pega o nome do hotel e joga na variavel, depois compara com o que o o usuario cadastrado e printa as amenities e o preço total
            name_attraction = x.get("name")
            print(name_attraction)
            if (name_attraction == str(attraction_name)):
                print(name_attraction)
                price = x.get("price").get("amount")
                print(price)
                return str(price)
            else:
                 print("Bruh")
    except ResponseError as error:
        print(error)

#stack 
def stack_filtering(rating_filter, hotels_to_filter):
    #init
    stack = []
    for hotel in hotels_to_filter:
        if hotel.hotel_rating == rating_filter:
            #push
            stack.append(hotel)
    return stack

#queue
def dequeue (favorites):
    del favorites[0]
    return favorites

def queue(log_human, username):
    queue = []
    if log_human.fav1 is not None:
        queue.append(log_human.fav1)
        if log_human.fav2 is not None:
            queue.append(log_human.fav2)
            if log_human.fav3 is not None:
                queue.append(log_human.fav3)
                if log_human.fav4 is not None:
                    queue.append(log_human.fav4)
                    if log_human.fav5 is not None:
                        queue.append(log_human.fav5)
                        queue = dequeue(queue)
                        queue.append(username)
                        log_human.fav1 = queue[0]
                        log_human.fav2 = queue[1]
                        log_human.fav3 = queue[2]
                        log_human.fav4 = queue[3]
                        log_human.fav5 = queue[4]
                        log_human.save()
                    else:
                        queue.append(username)
                        log_human.fav5 = queue[4]
                        log_human.save()
                else:
                    queue.append(username)
                    log_human.fav4 = queue[3]
                    log_human.save()
            else:
                queue.append(username)
                log_human.fav3 = queue[2]
                log_human.save()
        else:
            queue.append(username)
            log_human.fav2 = queue[1]
            log_human.save()
    else:
        print(username)
        queue.append(username)
        log_human.fav1 = queue[0]
        log_human.save()
        print(log_human.fav1)
    return
def len_queue ( favorites ):
    return favorites._size

def is_empty ( favorites ):
    return favorites._size == 0

def is_full ( favorites ):
    return favorites._size == 5

def first ( favorites ):
    return favorites._data [ favorites._front ]


def enqueue ( favorites, hotel_name ):    
    result = favorites.append(hotel_name)
    return result

class Node:
    def __init__(self,initdata):
        self.data = initdata
        self.next = None

    def getData(self):
        return self.data

    def getNext(self):
        return self.next

    def setData(self,newdata):
        self.data = newdata

    def setNext(self,newnext):
        self.next = newnext


class UnorderedList:

    def __init__(self):
        self.head = None

    def isEmpty(self):
        return self.head == None

    def add(self,item):
        temp = Node(item)
        temp.setNext(self.head)
        self.head = temp

    def size(self):
        current = self.head
        count = 0
        while current != None:
            count = count + 1
            current = current.getNext()

        return count

    def search(self,item):
        current = self.head
        found = False
        while current != None and not found:
            if current.getData() == item:
                found = True
            else:
                current = current.getNext()

        return found

    def getData(self):
        lista = []
        current = self.head
        while current != None:
            node = current.getData()
            lista.append(node)
            current = current.getNext()
        return lista

    def remove(self,item):
        current = self.head
        previous = None
        found = False
        while not found:
            if current.getData() == item:
                found = True
            else:
                previous = current
                current = current.getNext()

        if previous == None:
            self.head = current.getNext()
        else:
            previous.setNext(current.getNext())

mylist = UnorderedList()
