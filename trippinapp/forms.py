from django import forms
from .models import Trip
from .models import Register_human_user
from .models import Register_hotel_user
from .models import Register_restaurant_user
from .models import Register_attraction_user
from .models import *
from .models import Post
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Field 
from crispy_forms.bootstrap import (
    PrependedText, PrependedAppendedText, FormActions
)

class Post_Form(forms.ModelForm):    

    class Meta:
        model = Trip
        fields = ['author', 
                  'text', 
                  'created_date', 
        ]

class Register_human_user_form(forms.ModelForm):
    class Meta:
        model = Register_human_user
        fields = [
            'human_username',
            'human_adress',
            'human_birthday',
            'human_email',
            'human_password',
            'human_image',
        ]

class Register_hotel_user_form(forms.ModelForm):
    class Meta:
        model = Register_hotel_user
        fields = [
            'hotel_name',
            'hotel_adress',
            'hotel_email',
            # 'hotel_list',
            'hotel_password',
        ]

class Register_restaurant_user_form(forms.ModelForm):
    class Meta:
        model = Register_restaurant_user
        fields = [
            'restaurant_name',
            'restaurant_adress',
            'restaurant_cuisines',
            'restaurant_email',
            'restaurant_password',
            'restaurant_special_diets',
            'restaurant_phone_number',
            'restaurant_business_hours',
        ]

class Register_attraction_user_form(forms.ModelForm):
    class Meta:
        model = Register_attraction_user
        fields = [
            'attraction_name',
            'attraction_adress',
            'attraction_email',
            'attraction_password',
            'attraction_business_hours',
            'attraction_phone_number',
        ]

class PostForm(forms.ModelForm):
    helper = FormHelper()
    helper.form_method = 'POST'
    helper.add_input(Submit('Post', 'Post', css_class='btn-primary'))

    class Meta:
        model = Post 
        fields = [
            'image',
            'caption'
        ]

class CommentForm(forms.ModelForm):
    class Meta:
        model = Comment
        fields = ('text',)