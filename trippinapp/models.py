from django.db import models
from django.conf import settings
from django.utils import timezone
from django.urls import reverse
from datetime import datetime
from django.core.validators import (RegexValidator, MinLengthValidator, MaxLengthValidator, MinValueValidator,MaxValueValidator, EmailValidator)


# Create your models here.

class Trip(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.created_date

class Register_human_user(models.Model):
    human_username = models.TextField(blank=True, null=True, default = 'space cowboy')
    human_adress=models.TextField(blank=True, null=True)
    human_birthday=models.TextField(blank=True, null=True)
    human_email = models.EmailField(blank=False, null=False, default = 'space_cowboy@email.com', validators=[EmailValidator()])
    human_password = models.TextField(blank=False, null=False, default = '123456')
    human_image = models.ImageField(upload_to='images', null =True)
    fav1 =  models.TextField(blank = True, null=True)
    fav2 =  models.TextField(blank = True, null=True)
    fav3 =  models.TextField(blank = True, null=True)
    fav4 =  models.TextField(blank = True, null=True)
    fav5 =  models.TextField(blank = True, null=True)

    def __str__(self):
        return self.human_username

class Register_hotel_user(models.Model):
    hotel_name = models.TextField()
    hotel_adress = models.TextField()
    hotel_price = models.TextField()
    hotel_email = models.EmailField(blank=False, null=False, validators=[EmailValidator()])
    hotel_rating = models.TextField(blank=False, null=False,default = '0')
    hotel_password = models.TextField(blank=True, null=True, default = '123456')
    hotel_amenities = models.TextField()
    hotel_phone_number = models.TextField()
    hotel_image = models.ImageField(upload_to='images', null =True, default = 'default.jpg')

    def __str__(self):
        return self.hotel_name

class Campo_lista(models.Model):
    campo_lista = models.TextField()
    hotel = models.ForeignKey(Register_hotel_user, on_delete=models.CASCADE)

    def __str__(self):
        return self.campo_lista

class Register_restaurant_user(models.Model):
    restaurant_name = models.TextField()
    restaurant_adress = models.TextField()
    restaurant_cuisines = models.TextField()
    restaurant_email = models.EmailField(blank=False, null=False, validators=[EmailValidator()])
    restaurant_password = models.TextField(blank=True, null=True, default = '123456')
    restaurant_special_diets = models.TextField()
    restaurant_phone_number = models.TextField()
    restaurant_business_hours = models.TextField()
    tags = models.TextField(default="howdy")
    restaurant_image = models.ImageField(upload_to='images', null =True, default = 'default.jpg')

    def __str__(self):
        return self.restaurant_name

class Campo_lista_restaurante(models.Model):
    campo_lista = models.TextField()
    restaurante = models.ForeignKey(Register_restaurant_user, on_delete=models.CASCADE)

    def __str__(self):
        return self.campo_lista

class Register_attraction_user(models.Model):
    attraction_name = models.TextField()
    attraction_adress = models.TextField()
    attraction_paid = models.TextField()
    attraction_email = models.EmailField(blank=False, null=False, validators=[EmailValidator()])
    attraction_password = models.TextField(blank=True, null=True, default = '123456')
    attraction_business_hours = models.TextField()
    attraction_phone_number = models.TextField()
    attraction_image = models.ImageField(upload_to='images', null =True, default = 'default.jpg')
    
    def __str__(self):
        return self.attraction_name

class Campo_lista_atracao(models.Model):
    campo_lista = models.TextField()
    atracao = models.ForeignKey(Register_attraction_user, on_delete=models.CASCADE)

    def __str__(self):
        return self.campo_lista
    
#def publish(self):
#    self.published_date = timezone.now()
#    self.save()

class Post(models.Model):
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='posts')
    image = models.ImageField(default='default.png', blank=True)
    caption = models.TextField()
    likes = models.ManyToManyField(settings.AUTH_USER_MODEL, blank=True, related_name='post_likes')
    created_date = models.DateTimeField(default=timezone.now)
    saved   = models.BooleanField(default=False)  

    def get_absolute_url(self):
        return reverse('post_detail', kwargs={"id":self.id})
    
    def __str__(self):
        return self.caption 

class Comment(models.Model):
    post = models.ForeignKey('trippinapp.Post', on_delete=models.CASCADE, related_name='comments')
    author = models.ForeignKey('auth.User', on_delete=models.CASCADE, related_name='comments')
    text = models.TextField()
    created_date = models.DateTimeField(default=timezone.now)
    approved_comment = models.BooleanField(default=False)

    def approve(self):
        self.approved_comment = True
        self.save()

    def __str__(self):
        return self.text


