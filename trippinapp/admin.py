from django.contrib import admin
from .models import Trip
from .models import Register_human_user
from .models import Register_hotel_user
from .models import Register_restaurant_user
from .models import Register_attraction_user
from .models import Post, Comment, Campo_lista, Campo_lista_atracao, Campo_lista_restaurante

admin.site.register(Trip)
admin.site.register(Post)
admin.site.register(Comment)
admin.site.register(Register_human_user)
admin.site.register(Register_hotel_user)
admin.site.register(Register_restaurant_user)
admin.site.register(Register_attraction_user)
admin.site.register(Campo_lista)
admin.site.register(Campo_lista_restaurante)
admin.site.register(Campo_lista_atracao)


# Register your models here.
