import sys
sys.setrecursionlimit(1500)
from django.shortcuts import render, redirect, get_object_or_404
from .models import Trip
from .models import Register_human_user
from .models import Register_hotel_user
from .models import Register_restaurant_user
from .models import Register_attraction_user

import json
from django.views.generic import (
    CreateView,
    DetailView,
    ListView,
    UpdateView,
    DeleteView,
    RedirectView,
)
from .models import Post 
from .forms import PostForm 

from django.http import HttpResponse, JsonResponse
#importando tudo da utils, pois nela está os métodos de handling dos inputs
from .utils import *
from django.template import loader
#crud
from django.views.generic import ListView, CreateView, UpdateView, DeleteView
from django.urls import reverse_lazy
#timezone
from django.utils import timezone
#por causa do erro de csrf
from django.views.decorators.csrf import csrf_exempt
#forms
from .forms import Post_Form, CommentForm
from .forms import Register_human_user_form
from .forms import Register_hotel_user_form
from .forms import Register_restaurant_user_form
from .forms import Register_attraction_user_form

#Creting user?
from django.contrib.auth.models import User
#write csv
from djqscsv import render_to_csv_response
from djqscsv import write_csv
#para adicionar querysets em um csv
import csv
#para adicionar uma linha em resultados.csv
from csv import writer
#para procurar números em uma string
import re
#para converter string para datetime
from datetime import datetime
#authenticator:
from django.contrib.auth import authenticate, login, logout
#para retornar erro
from django.contrib import messages
#login required:
from django.contrib.auth.decorators import login_required
#logout

from django.core.serializers import serialize
#Lógica do site: Faz um request pra model e passa para um template. 

@csrf_exempt 
def register_human_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_human_user_form(request.POST, request.FILES)
    if request.method == 'POST' and 'register' in request.POST:
        print(form.is_valid())
        if form.is_valid():
            human_username = request.POST.get("human_username", None)
            human_adress= request.POST.get("human_adress", None)
            human_birthday= request.POST.get("human_birthday", None)
            human_email = request.POST.get("human_email", None)
            human_password = request.POST.get("human_password", None)
            human_image = request.POST.get("human_image", None)
            new_user = User.objects.create_user(human_username, human_email, human_password)
            new_user.save()
            # human_user = Register_human_user(human_username=human_username, human_adress=human_adress, human_birthday=human_birthday, human_email=human_email, human_password=human_password, human_image =human_image)
            # human_user.save()
            form.save()
            #salvando todos os querysets em um csv
            qs = Register_human_user.objects.all()
            with open('human_users_list.csv', 'wb') as csv_file:
                write_csv(qs, csv_file)
            return redirect('/')
        else:
            messages.error(request, form.errors.as_text()) 
            return redirect('/register_human_user')
    else:
        form = Register_human_user_form()
    return render(request, 'trippinapp/register_human_user.html') 
    
def register_hotel_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_hotel_user_form(request.POST)
    if request.method == 'POST' and 'register' in request.POST:
        try:
            if form.is_valid():
                hotel_name = request.POST.get("hotel_name", None)
                hotel_adress= request.POST.get("hotel_adress", None)
                #hotel_price= request.POST.get("hotel_price", None)
                hotel_email = request.POST.get("hotel_email", None)
                hotel_password = request.POST.get("hotel_password", None)
                #hotel_amenities = request.POST.get("hotel_amenities", None)
                #hotel_phone_number = request.POST.get("hotel_phone_number", None)
                new_user = User.objects.create_user(hotel_name, hotel_email, hotel_password)
                new_user.save()
                hotel_amenities, hotel_price, hotel_phone_number, hotel_rating = get_hotel(hotel_name, hotel_adress)
                #get_hotel(hotel_name, hotel_adress)
                hotel_user = Register_hotel_user(hotel_name=hotel_name, hotel_adress=hotel_adress, hotel_price=hotel_price, hotel_email=hotel_email, hotel_password = hotel_password, hotel_amenities=hotel_amenities, hotel_phone_number = hotel_phone_number, hotel_rating = hotel_rating)
                hotel_user.save()
                #salvando todos os querysets em um csv
                qs = Register_hotel_user.objects.all()
                mylist = UnorderedList()
                mylist.add(hotel_name)
                mylist.add(hotel_adress)
                mylist.add("Hotel")
                lista = mylist.getData()
                campo_lista1 = Campo_lista(campo_lista=lista[0], hotel=hotel_user)
                campo_lista1.save()
                campo_lista2 = Campo_lista(campo_lista=lista[1], hotel=hotel_user)
                campo_lista2.save()
                campo_lista3 = Campo_lista(campo_lista=lista[2], hotel=hotel_user)
                campo_lista3.save()
                print(hotel_user.campo_lista_set.all())
                with open('human_hotel_list.csv', 'wb') as csv_file:
                    write_csv(qs, csv_file)
                return redirect('/')
            else:
                messages.error(request, form.errors.as_text()) 
                return redirect('/register_hotel_user')
        except Exception as identifier:
            messages.error(request, 'Hotel não cadastrado na API')   
            return redirect('/register_hotel_user')
    else:
        form = Register_hotel_user_form()
    return render(request, 'trippinapp/register_hotel_user.html')   

# def change_hotel_image(request):

#     #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
#     #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
#     form = Change_hotel_image_form(request.POST)
#     if request.method == 'POST' and 'change_image' in request.POST:
#         if form.is_valid():
#             hotel_image = request.POST.get("hotel_image", None)
#             form.save()
#             #salvando todos os querysets em um csv
#             qs = Change_hotel_image.objects.all()
#             with open('hotel_users_list.csv', 'wb') as csv_file:
#                 write_csv(qs, csv_file)
#         return redirect('/')
#     else:
#         form = Change_hotel_image_form()
#     return render(request, 'trippinapp/human_user.html') 

def register_restaurant_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_restaurant_user_form(request.POST)
    if request.method == 'POST' and 'register' in request.POST:
        try:
            if form.is_valid():
                restaurant_name = request.POST.get("restaurant_name", None)
                restaurant_adress= request.POST.get("restaurant_adress", None)
                restaurant_cuisines= request.POST.get("restaurant_cuisines", None)
                restaurant_email = request.POST.get("restaurant_email", None)
                restaurant_password = request.POST.get("restaurant_password", None)
                restaurant_special_diets = request.POST.get("restaurant_special_diets", None)
                restaurant_phone_number = request.POST.get("restaurant_phone_number", None)
                restaurant_business_hours = request.POST.get("restaurant_business_hours", None)
                new_user = User.objects.create_user(restaurant_name, restaurant_email, restaurant_password)
                new_user.save()
                citycode = json.loads(restaurant_adress)
                latitude = citycode["lat"]
                longitude = citycode["long"]
                tags = get_restaurant(restaurant_name, latitude, longitude)
                restaurant_user = Register_restaurant_user(restaurant_name=restaurant_name, restaurant_adress=restaurant_adress, restaurant_cuisines=restaurant_cuisines, restaurant_email=restaurant_email, restaurant_password=restaurant_password, restaurant_special_diets= restaurant_special_diets, restaurant_phone_number = restaurant_phone_number, restaurant_business_hours = restaurant_business_hours, tags = tags)
                restaurant_user.save()
                #salvando todos os querysets em um csv
                qs = Register_restaurant_user.objects.all()
                mylist = UnorderedList()
                mylist.add(restaurant_name)
                mylist.add(restaurant_adress)
                mylist.add("Restaurante")
                lista = mylist.getData()
                campo_lista1 = Campo_lista_restaurante(campo_lista=lista[0], restaurante=restaurant_user)
                campo_lista1.save()
                campo_lista2 = Campo_lista_restaurante(campo_lista=lista[1], restaurante=restaurant_user)
                campo_lista2.save()
                campo_lista3 = Campo_lista_restaurante(campo_lista=lista[2], restaurante=restaurant_user)
                campo_lista3.save()
                print(hotel_user.campo_lista_set.all())
                with open('human_restaurant_list.csv', 'wb') as csv_file:
                    write_csv(qs, csv_file)
                return redirect('/')
            else:
                messages.error(request, form.errors.as_text()) 
                return redirect('/register_restaurant_user')
        except Exception as identifier:
            messages.error(request, 'Restaurante não cadastrado na API')   
            return redirect('/register_restaurant_user')
    else:
        form = Register_restaurant_user_form()
    return render(request, 'trippinapp/register_restaurant_user.html')    

def register_attraction_user(request):

    #Verifica se o botão "Post" foi apertado. Então salva os inputs do request, bem como os resultados 
    #dos métodos que estão na utils.py e salva na Model. Também salva em um .csv e depois renderiza com o template.
    form = Register_attraction_user_form(request.POST)
    if request.method == 'POST' and 'register' in request.POST:
        try:
            if form.is_valid():
                attraction_name = request.POST.get("attraction_name", None)
                attraction_adress= request.POST.get("attraction_adress", None)
                attraction_email = request.POST.get("attraction_email", None)
                attraction_password = request.POST.get("attraction_password", None)
                attraction_phone_number = request.POST.get("attraction_phone_number", None)
                attraction_business_hours = request.POST.get("attraction_business_hours", None)
                new_user = User.objects.create_user(attraction_name, attraction_email, attraction_password)
                new_user.save()
                citycode = json.loads(attraction_adress)
                latitude = citycode["lat"]
                longitude = citycode["long"]
                attraction_paid = get_attraction(attraction_name, latitude, longitude)
                attraction_user = Register_attraction_user(attraction_name=attraction_name, attraction_adress=attraction_adress, attraction_paid=attraction_paid, attraction_email=attraction_email, attraction_password=attraction_password, attraction_phone_number = attraction_phone_number, attraction_business_hours= attraction_business_hours)
                attraction_user.save()
                #salvando todos os querysets em um csv
                qs = Register_attraction_user.objects.all()
                mylist = UnorderedList()
                mylist.add(attraction_name)
                mylist.add(attraction_adress)
                mylist.add("Atracao")
                lista = mylist.getData()
                campo_lista1 = Campo_lista_atracao(campo_lista=lista[0], atracao=attraction_user)
                campo_lista1.save()
                campo_lista2 = Campo_lista_atracao(campo_lista=lista[1], atracao=attraction_user)
                campo_lista2.save()
                campo_lista3 = Campo_lista_atracao(campo_lista=lista[2], atracao=attraction_user)
                campo_lista3.save()
                print(hotel_user.campo_lista_set.all())
                with open('human_attraction_list.csv', 'wb') as csv_file:
                    write_csv(qs, csv_file)
                return redirect('/')
            else:
                messages.error(request, form.errors.as_text()) 
                return redirect('/register_attraction_user')
        except Exception as identifier:
            messages.error(request, 'Atração não cadastrado na API')   
            return redirect('/register_attraction_user')
    else:
        form = Register_attraction_user_form()
    return render(request, 'trippinapp/register_attraction_user.html')  


#views da página de login:
def login_user(request):
    if request.user.is_authenticated:
        return redirect('/page_hotel/')
    return render(request, 'trippinapp/login.html', {})

#views da autenticação:
def submit_login(request):
    if request.POST:
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect ('/page_hotel/')
        else:
            messages.error(request, 'Usuário ou senha inválido.')
    return redirect('/')

def logout_request(request):
    logout(request)
    messages.info(request, "Usuário deslogado com sucesso!")
    return redirect("/")

@login_required(login_url='/')
def get_user_profile(request, username):
    fav_hotel = User.objects.get(username = username)
    human = request.user.username
    log_human = Register_human_user.objects.get(human_username= human)
    if request.method == 'POST' and 'Adicionar aos Favoritos' in request.POST:
        queue(log_human, username)
    try:
        user = User.objects.get(username = username)
        posts = Post.objects.all().filter(author=user).order_by('-created_date')
        print(posts)
        try:
            user = Register_hotel_user.objects.get(hotel_name = user)
            user_location = user.hotel_adress
            user_email = user.hotel_email
            user_birthday = user.hotel_price
        except Register_hotel_user.DoesNotExist:
            try:
                user = Register_restaurant_user.objects.get(restaurant_name = user)
                user_location = user.restaurant_adress
                user_email = user.restaurant_email
                user_birthday = user.tags
            except Register_restaurant_user.DoesNotExist:
                user = Register_attraction_user.objects.get(attraction_name = user)
                user_location = user.attraction_adress
                user_email = user.attraction_email
                user_birthday = user.attraction_paid
    except User.DoesNotExist:
        return redirect('/page_hotel/')   
    return render(request, 'trippinapp/user_profile.html', {"user_model":user,'user_location':user_location, 'user_email':user_email, 'user_birthday':user_birthday, "posts":posts})

@login_required(login_url='/')
#views da página de listagem dos processos:    
def page_hotel(request):
    hotels = Register_hotel_user.objects.filter().order_by('id')
    #FILTRAR POR AVALIAÇÃO USANDO PILHA
    if request.method == 'POST' and 'filter' in request.POST:
        rating_filter = request.POST.get("rating_filter", None)
        hotels_to_filter = Register_hotel_user.objects.all()
        filter_result = stack_filtering(rating_filter, hotels_to_filter)
        #hotels_to_filter = Register_hotel_user.objects.filter(hotel_rating=rating_filter)
        return render(request, 'trippinapp/page_hotel.html', {'hotels':filter_result})
    return render(request, 'trippinapp/page_hotel.html', {'hotels':hotels})

@login_required(login_url='/')
#views da página de listagem dos processos:    
def init_page(request):
    #FILTRAR POR AVALIAÇÃO USANDO PILHA
    if request.method == 'POST' and 'Escolher' in request.POST:
        type_user = request.POST.get("user_type", None)
        return render(request, 'trippinapp/init_page.html', {'busca':None, 'tipo':type_user})
    if request.method == 'POST' and 'Adicionar1' in request.POST:
        hotels = Register_hotel_user.objects.all()
        type_user = request.POST.get("user_type", None)
        adress_user = request.POST.get("hotel_adress", None)
        i = 0
        listas = []
        results = []
        for hotel in hotels:
            user_hotel = hotel
            campos = user_hotel.campo_lista_set.all()
            mylist = UnorderedList()
            print(campos[0])
            print(campos[1])
            print(campos[2])
            mylist.add(campos[2].campo_lista)
            mylist.add(campos[1].campo_lista)
            mylist.add(campos[0].campo_lista)
            listas.append(mylist)
        print(listas)
        for lista in listas:
            if ((lista.search(type_user) == True) and (lista.search(adress_user) == True)):
                lista1 = lista.getData()
                hotel_try =   Register_hotel_user.objects.get(hotel_name = lista1[2])
                results.append(hotel_try)
                print('nwdjdnwjndwj')
        print(results)    
        return render(request, 'trippinapp/init_page.html', {'busca':results, 'tipo':type_user})
    if request.method == 'POST' and 'Adicionar2' in request.POST:
        print('Estou no adcionar 2')
        restaurants = Register_restaurant_user.objects.all()
        type_user = request.POST.get("user_type", None)
        adress_user = request.POST.get("restaurant_adress", None)
        i = 0
        listas = []
        results = []
        for restaurant in restaurants:
            print(restaurant.restaurant_name)
            user_restaurant = restaurant
            campos = user_restaurant.campo_lista_restaurante_set.all()
            mylist = UnorderedList()
            # print(campos[0])
            # print(campos[1])
            # print(campos[2])
            mylist.add(campos[2].campo_lista)
            mylist.add(campos[1].campo_lista)
            mylist.add(campos[0].campo_lista)
            listas.append(mylist)
        # print(listas)
        # print(type_user)
        # print(adress_user)
        for lista in listas:
            if ((lista.search(type_user) == True) and (lista.search(adress_user) == True)):
                lista1 = lista.getData()
                print(lista1[2])
                restaurant_try = Register_restaurant_user.objects.get(restaurant_name = lista1[2])
                results.append(restaurant_try)
        # print(results)    
        return render(request, 'trippinapp/init_page.html', {'busca':results, 'tipo':type_user})
    if request.method == 'POST' and 'Adicionar3' in request.POST:
        print('Estou no adcionar 2')
        attractions = Register_attraction_user.objects.all()
        type_user = request.POST.get("user_type", None)
        adress_user = request.POST.get("atracao_adress", None)
        i = 0
        listas = []
        results = []
        for attraction in attractions:
            print(attraction.attraction_name)
            user_attraction = attraction
            campos = user_attraction.campo_lista_atracao_set.all()
            mylist = UnorderedList()
            # print(campos[0])
            # print(campos[1])
            # print(campos[2])
            mylist.add(campos[2].campo_lista)
            mylist.add(campos[1].campo_lista)
            mylist.add(campos[0].campo_lista)
            listas.append(mylist)
        # print(listas)
        # print(type_user)
        # print(adress_user)
        for lista in listas:
            if ((lista.search(type_user) == True) and (lista.search(adress_user) == True)):
                lista1 = lista.getData()
                print(lista1[2])
                attraction_try = Register_attraction_user.objects.get(attraction_name = lista1[2])
                results.append(attraction_try)
        # print(results)    
        return render(request, 'trippinapp/init_page.html', {'busca':results, 'tipo':type_user})
    return render(request, 'trippinapp/init_page.html', {'busca':None, 'tipo':None})

@login_required(login_url='/')
#views da página de listagem dos processos:    
def page_restaurant(request):
    restaurants = Register_restaurant_user.objects.filter().order_by('restaurant_name')
    return render(request, 'trippinapp/page_restaurant.html', {'restaurants':restaurants})

@login_required(login_url='/')
#views da página de listagem dos processos:    
def page_attractions(request):
    attractions = Register_attraction_user.objects.filter().order_by('attraction_name')
    return render(request, 'trippinapp/page_attractions.html', {'attractions':attractions})

@login_required(login_url='/')
def delete_post(request, process_id):
    post_id = int(post_id)
    try:
        post_sel = Trip.objects.get(id = post_id)
    except Trip.DoesNotExist:
        return redirect('/post_list/')
    post_sel.delete()
    return redirect('/post_list/')


@login_required(login_url='/')
def human_user(request):
    username = None
    if request.user.is_authenticated:
        username = request.user.username
        user_id = request.user.id
        posts = Post.objects.all().filter(author=user_id).order_by('-created_date')
        print(posts)
        try:
            user = Register_human_user.objects.get(human_username = username)
            user_avatar = user.human_image
            user_location = user.human_adress
            user_email = user.human_email
            user_birthday = user.human_birthday
            user_fav1 = user.fav1
            user_fav2 = user.fav2
            user_fav3 = user.fav3
            user_fav4 = user.fav4
            user_fav5 = user.fav5
        except Register_human_user.DoesNotExist:
            try:
                user = Register_hotel_user.objects.get(hotel_name = username)
                user_avatar = user.hotel_image
                user_location = user.hotel_adress
                user_email = user.hotel_email
                user_birthday = user.hotel_price
            except Register_hotel_user.DoesNotExist:
                try:
                    user = Register_attraction_user.objects.get(attraction_name = username)
                    user_avatar = user.attraction_image
                    user_location = user.attraction_adress
                    user_email = user.attraction_email
                    user_birthday = user.attraction_paid
                except Register_attraction_user.DoesNotExist:
                    try:
                        user = Register_restaurant_user.objects.get(restaurant_name = username)
                        user_avatar = user.restaurant_image
                        user_location = user.restaurant_adress
                        user_email = user.restaurant_email
                        user_birthday = user.tags
                    except Register_restaurant_user.DoesNotExist:
                        return redirect('/page_hotel/')
        return render(request, 'trippinapp/human_user.html', {'username':username, 'user_avatar':user_avatar, 'user_location':user_location, 'user_email':user_email, 'user_birthday':user_birthday, 'user_fav1':user_fav1, 'user_fav2':user_fav2, 'user_fav3':user_fav3, 'user_fav4':user_fav4, 'user_fav5':user_fav5,'posts': posts})


class PostListView(ListView):
    template_name = 'trippinapp/post_list.html'
    queryset = Post.objects.all().filter(created_date__lte=timezone.now()).order_by('-created_date')
    #print(queryset)
    context_object_name = 'posts'

#class PostDetailView(request, id):
    #template_name = 'trippinapp/details.html'
    #queryset = Post.objects.all().filter(created_date__lte=timezone.now())
    
    #def get_object(self):
    #    id_ = self.kwargs.get("id")
    #    return get_object_or_404(Post, id=id_)

def PostDetailView(request, id):
    #id_ = self.kwargs.get("id")
    post = get_object_or_404(Post, id =id)
    # List of active comments for this post
    comments = post.comments.all()
    print(comments)
    new_comment = None

    if request.method == 'POST':
        # A comment was posted
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            # Create Comment object but don't save to database yet          
            new_comment = comment_form.save(commit=False)
            # Assign the current post to the comment
            new_comment.post = post
            # Save the comment to the database
            new_comment.save()
    else:
        comment_form = CommentForm()                   
    return render(request,
                    'trippinapp/details.html',
                    {'post': post,
                    'comments': comments,
                    'new_comment': new_comment,
                    'comment_form': comment_form})


class PostCreateView(CreateView):
    template_name = 'trippinapp/trippin_post.html'
    form_class = PostForm
    queryset = Post.objects.all()     #.filter(created_date__lte=timezone.now())
    #success_url = '/'
    def form_valid(self, form):
        print(form.cleaned_data)
        form.instance.author = self.request.user 
        return super().form_valid(form)

class PostUpdateView(UpdateView):
    template_name = 'trippinapp/trippin_post.html'
    form_class = PostForm 

    def get_object(self):
        id_ = self.kwargs.get("id")
        return get_object_or_404(Post, id=id_)

    def form_valid(self, form):
        # form.instance.author = self.request.user      
        return super().form_valid(form) 

class PostDeleteView(DeleteView):
    template_name = 'trippinapp/delete.html'

    def get_object(self):
        id_=self.kwargs.get("id")
        return get_object_or_404(Post, id=id_)

    def get_success_url(self):
        return reverse('post_list')

def saved_posts(request):
    posts = Post.objects.filter(saved=True)
    context = {'saved_posts': posts}
    return render(request, 'trippinapp/saved_posts.html', context)

class PostLikeToggle(RedirectView):
    def get_redirect_url(self, *args, **kwargs):
        id_ = self.kwargs.get("id")
        obj = get_object_or_404(Post, id=id_)
        url_ = obj.get_absolute_url()
        user = self.request.user 
        if user.is_authenticated:
            if user in obj.likes.all():
                obj.likes.remove(user)
            else:
                obj.likes.add(user) 
        return url_ 


